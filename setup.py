#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
from setuptools import setup, find_packages
import os


YDB_DIST = os.environ.get("ydb_dist")
if YDB_DIST is None:
    print(
        "Error: $ydb_dist is not set. Try running `source $(pkg-config --variable=prefix yottadb)/ydb_env_set`"
    )
    exit(1)


setup(
    name="YDBApron",
    version="0.1.0",
    packages=find_packages(include=["YDBApron", "YDBApron.*"]),
    install_requires=["yottadb"],
    tests_require=["pytest"],
)
