#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
import os
import pathlib
import shutil
import pytest
import subprocess
import yottadb

YDB_DIST = os.environ["ydb_dist"]
BUILD_DIR = "/build/tests/"
TEST_DIR = "/tests/"
CWD = os.getcwd()


def create_db() -> str:
    """
    Create and setup a test database relative to the current working directory.
    """
    db = {}

    db["dir"] = CWD + BUILD_DIR + "/"

    # Create test directory
    if os.path.exists(db["dir"]):
        shutil.rmtree(db["dir"])
    pathlib.Path(db["dir"]).mkdir(parents=True, exist_ok=True)

    # Set ydb_gbldir environment variable for use by yottadb module
    db["gld"] = db["dir"] + "test.gld"
    os.environ["ydb_gbldir"] = db["gld"]

    # Create YottaDB database and global directory and populate with test data.
    subprocess.run([f"{CWD}/tests/createdb.sh", f"{db['dir'] + 'test.dat'}", f"{CWD + TEST_DIR + '/' + 'testdata.zwr'}"])

    return db


def teardown_db(db: dict):
    """
    Delete a database and associated files previously created by `setup_db()`.
    Accepts a `dict` returned by `setup_db()` containing database information
    uses it to delete the directory.
    """
    shutil.rmtree(db["dir"])


def print_db():
    """
    Print the values of all nodes in a database.
    """

    for gvar in yottadb.subscripts("^%"):
        for node in yottadb.nodes(gvar):
            print(f"{gvar}{node}={yottadb.get(gvar, node)}")


@pytest.fixture(scope="function")
def test_db():
    """
    Initializes a new database and populates it with regular, i.e. non-randomized
    dummy data prior to test execution. Then, deletes the database when testing
    is complete. For use with individual test functions.
    """

    db = create_db()

    yield

    teardown_db(db)
