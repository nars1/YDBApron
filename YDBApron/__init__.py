#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
import os
import sys
import signal

from flask import Flask, render_template
from flask_cors import CORS

from . import recipes, ingredients, schedules


# Create signal handler for SIGINT so that CTRL-C can be used to terminate the application
def handle_sigint(sig, frame):
    sys.exit(0)


signal.signal(signal.SIGINT, handle_sigint)


def create_app(config=None):
    # Create a new application instance
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
    )

    # Enable CORS
    CORS(app, resources={r'/*': {'origins': '*'}})

    if config is None:
        # Load application configuration from file
        app.config.from_pyfile('config.py', silent=True)
    else:
        # Load configuration from argument
        app.config.from_mapping(config)

    # Ensure app folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Application routes
    @app.route('/')
    def home():
        return render_template('index.html')
    # Recipes
    app.register_blueprint(recipes.blueprint)
    # Ingredients
    app.register_blueprint(ingredients.blueprint)
    # Production Schedules
    app.register_blueprint(schedules.blueprint)

    return app
