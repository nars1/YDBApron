#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
from wtforms import Form, FieldList, FormField, StringField, IntegerField, DecimalField, SelectField, TextAreaField
from wtforms.validators import InputRequired, Length, Optional

from YDBApron.globals import MAX_PRODUCT_SPECS, MAX_INGREDIENTS, MAX_RECIPES


class IngredientForm(Form):
    # TODO: Remove Optional() validator when client-side validation is added
    ingredient_name = StringField("Name", [Length(min=2, max=100, message="Name must be between 4 and 100 characters long.")], render_kw={"placeholder": "e.g. whole wheat flour"})
    ingredient_category = StringField("Category", [Length(min=2, max=100, message="Category must be between 4 and 100 characters long.")], render_kw={"placeholder": "e.g. flour"})
    ingredient_amount = DecimalField("Amount", default=0, render_kw={"placeholder": "0"})
    ingredient_unit = SelectField("Unit", choices=[("g", "grams"), ("kg", "kilograms"), ("oz", "ounces"), ("lb", "pounds"), ("floz", "fluid ounces"), ("mL", "milliliters"), ("L", "liters"), ("one", "single item")])
    ingredient_price = DecimalField("Price", default=0, render_kw={"placeholder": "0"})
    ingredient_manufacturer = StringField("Manufacturer", [Length(min=2, max=100, message="Manufacturer must be between 4 and 100 characters long.")], render_kw={"placeholder": "e.g. King Arthur"})
    ingredient_vendor = StringField("Vendor", [Optional(), Length(min=2, max=100, message="Vendor must be between 4 and 100 characters long.")], render_kw={"placeholder": "e.g. Amazon"})


class RecipeIngredientForm(Form):
    # TODO: Remove Optional() validator when client-side validation is added
    ingredient_name = StringField("Name", [Optional(), Length(min=4, max=100)], render_kw={"placeholder": "e.g. whole wheat flour"})
    ingredient_category = StringField("Category", [Optional(), Length(min=4, max=100)], render_kw={"placeholder": "e.g. flour"})
    ingredient_amount = DecimalField("Weight/Volume", [Optional()], render_kw={"placeholder": "0"})
    ingredient_unit = SelectField("Unit", [Optional()], choices=[("g", "grams"), ("kg", "kilograms"), ("oz", "ounces"), ("lb", "pounds"), ("floz", "fluid ounces"), ("mL", "milliliters"), ("L", "liters"), ("one", "single item")])


class ProductSpecificationForm(Form):
    # TODO: Remove Optional() validator when client-side validation is added
    specification_format = StringField("Format Type", [Optional(), Length(min=4, max=100)], render_kw={"placeholder": "e.g. loaf, roll, etc."})
    specification_format_size = StringField("Format Size", [Optional()], render_kw={"placeholder": "e.g. large, small, etc."})
    specification_size = DecimalField("Size", [Optional()], render_kw={"placeholder": "0"})
    specification_unit = SelectField("Unit", [Optional()], choices=[("g", "grams"), ("kg", "kilograms"), ("oz", "ounces"), ("lb", "pounds"), ("floz", "fluid ounces"), ("mL", "milliliters"), ("L", "liters"), ("it", "single item")])


class RecipeForm(Form):
    recipe_name = StringField("Name", [Length(min=4, max=100), InputRequired()], render_kw={"placeholder": "e.g. Rustic Wheat Bread"})
    recipe_category = StringField("Category", [Length(min=4, max=100), InputRequired()], render_kw={"placeholder": "e.g. bread"})
    ingredients = FieldList(FormField(RecipeIngredientForm, [Optional()]), min_entries=MAX_INGREDIENTS)
    specifications = FieldList(FormField(ProductSpecificationForm, [Optional()]), min_entries=MAX_PRODUCT_SPECS)
    procedure = TextAreaField("Procedure", [Length(min=4, max=2000), InputRequired()], render_kw={"placeholder": "e.g. Step 1: ..."})


class ScheduleSpecificationForm(Form):
    specification_name = StringField("Specification Name", [Optional()])
    specification_size = StringField("Specification Size", [Optional()])
    specification_volume = DecimalField("Specification Volume", [Optional()], places=2, render_kw={"placeholder": 0})


class ScheduleRecipeForm(Form):
    # TODO: Remove Optional() validator when client-side validation is added
    recipe_name = StringField("Recipe Name", [Optional()])
    recipe_category = StringField("Recipe Category", [Optional()])
    specifications = FieldList(FormField(ScheduleSpecificationForm, [Optional()]), min_entries=6)
    supplementary_yield = IntegerField("Gross Yield", [Optional()], render_kw={"placeholder": 0})
    gross_yield = IntegerField("Gross Yield", [Optional()], render_kw={"placeholder": 0})
    gross_cost = DecimalField("Gross Yield", [Optional()], places=2, render_kw={"placeholder": 0})


class ScheduleForm(Form):
    schedule_name = StringField("Schedule Name", [Length(min=4, max=100), InputRequired()], render_kw={"placeholder": "e.g. Wednesday"})
    recipes = FieldList(FormField(ScheduleRecipeForm, [Optional()]), min_entries=MAX_RECIPES)
