#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################

GRAMS_PER_OUNCE = 28  # Approximate number of grams in an ounce
OUNCES_PER_POUND = 16  # Number of ounces in a pound
GRAMS_PER_MILLILITER = 1  # Number of grams in a milliliter. May be inaccurate for liquids other than water
MAX_PRODUCT_SPECS = 6  # Maximum number of product specifications that can be defined for a single recipe
MAX_INGREDIENTS = 200  # Maximum number of ingredients that can be stored. Needed for pre-allocation of form fields
MAX_RECIPES = 100  # Maximum number of recipes that can be stored. Needed for pre-allocation of form fields
