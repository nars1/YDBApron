# YDBApron

YDBApron is a simple recipe planning and costing application for organizing food and beverage preparation in small-scale kitchens, such as at home or in a small business or community organization.

YDBApron demonstrates one possible way to build web applications using a YottaDB database for persistence. In addition to YottaDB, YDBApron uses the following software stack:

+ Backend:
    - Web framework: Flask
+ Frontend:
    - Bootstrap

## Quickstart

YDBApron is not currently available on PyPI, and so must be run from source. To do so:

```sh
git clone https://gitlab.com/YottaDB/Demo/YDBApron.git
cd YDBApron
python3 -m venv .venv      # Create a new Python virtual environment ("venv")
source .venv/bin/activate  # Initialize the new venv
source $(pkg-config --variable=prefix yottadb)/ydb_env_set  # Set YottaDB environment variables
pip install yottadb flask flask_cors WTForms pytest  # Install prerequisites
export FLASK_APP=YDBApron
flask run --host=0.0.0.0
```

The YDBApron web application can then be accessed by pointing a browser to `localhost:5000`.
